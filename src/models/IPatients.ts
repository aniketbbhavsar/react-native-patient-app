export interface IPatients {
  id: string;
  name: string;
  gender: string;
  birthDate: Date;
  isForwarded: boolean;
  createdAt: Date;
  updatedAt: Date;
}
