export interface IPatient {
  id: string;
  name: string;
  gender: string;
  birthDate: Date;
  isForwarded: boolean;
  events: Event[];
  createdAt: Date;
  updatedAt: Date;
}

export interface Event {
  text: string;
  createdAt: Date;
}
