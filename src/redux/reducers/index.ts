import { combineReducers } from 'redux';
import forwardPatientReducer from './forwardPatientReducer';
import patientsReducer from './patientsReducer';
import singlePatientReducer from './singlePatientReducer';
const reducers = combineReducers({
  patients: patientsReducer,
  singlePatient: singlePatientReducer,
  patientForward: forwardPatientReducer,
});

export default reducers;

export type RootState = ReturnType<typeof reducers>; // to know type of information inside store
