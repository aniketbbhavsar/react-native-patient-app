import { singlePatientForwardTypes } from '../action types';
import {
  ForwardPatientActions,
  ForwardPatientState,
} from '../types/forwardPatientTypes';

const initialState: ForwardPatientState = {
  isPatientForwardPending: false,
  isPatientForward: false,
  isPatientForwardError: null,
};

const forwardPatientReducer = (
  state = initialState,
  action: ForwardPatientActions,
) => {
  switch (action.type) {
    case singlePatientForwardTypes.FORWARD_PATIENT_REQUEST: {
      return {
        ...state,
        isPatientForwardPending: true,
        isPatientForwardError: null,
      };
    }
    case singlePatientForwardTypes.FORWARD_PATIENT_REQUEST_SUCCESS: {
      return {
        ...state,
        isPatientForward: action.payload.isPatientForward,
        isPatientForwardPending: false,
      };
    }
    case singlePatientForwardTypes.FORWARD_PATIENT_REQUEST_FAIL: {
      return {
        ...state,
        isPatientForward: false,
        isPatientForwardPending: false,
        isPatientForwardError: action.payload.error,
      };
    }
    default:
      return {
        ...state,
      };
  }
};

export default forwardPatientReducer;
