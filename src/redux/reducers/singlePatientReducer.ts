import { singlePatientActionTypes } from '../action types';
import {
  SinglePatientState,
  SinglePatientActions,
} from '../types/singlePatientTypes';

const initialState: SinglePatientState = {
  pending: false,
  patient: null,
  error: null,
};

const singlePatientReducer = (
  state = initialState,
  action: SinglePatientActions,
) => {
  switch (action.type) {
    case singlePatientActionTypes.FETCH_SINGLE_PATIENT_REQUEST:
      return {
        ...state,
        pending: true,
      };
    case singlePatientActionTypes.FETCH_SINGLE_PATIENT_SUCCESS:
      return {
        ...state,
        pending: false,
        patient: action.payload.patient,
        error: null,
      };
    case singlePatientActionTypes.FETCH_SINGLE_PATIENT_PATIENT_FAILURE:
      return {
        ...state,
        pending: false,
        patient: null,
        error: action.payload.error,
      };

    default:
      return {
        ...state,
      };
  }
};

export default singlePatientReducer;
