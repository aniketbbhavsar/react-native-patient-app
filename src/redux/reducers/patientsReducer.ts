import { patientsActionTypes } from '../action types';
import { PatientsActions, PatientsState } from '../types/fetchAllPatientsTypes';

const initialState: PatientsState = {
  pending: false,
  patients: [],
  error: null,
};

const patientsReducer = (state = initialState, action: PatientsActions) => {
  switch (action.type) {
    case patientsActionTypes.FETCH_PATIENTS_REQUEST:
      return {
        ...state,
        pending: true,
      };
    case patientsActionTypes.FETCH_PATIENTS_SUCCESS:
      return {
        ...state,
        pending: false,
        patients: action.payload.patients,
        error: null,
      };
    case patientsActionTypes.FETCH_PATIENTS_FAILURE:
      return {
        ...state,
        pending: false,
        patients: [],
        error: action.payload.error,
      };
    default:
      return {
        ...state,
      };
  }
};

export default patientsReducer;
