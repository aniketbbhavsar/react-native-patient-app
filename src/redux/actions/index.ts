import { IPatient } from '../../models/IPatient';
import {
  patientsActionTypes,
  singlePatientForwardTypes,
} from '../action types';
import { singlePatientActionTypes } from '../action types';
import {
  FetchPatientsSuccess,
  FetchPatientsFailure,
  FetchPatientsFailurePayload,
  FetchPatientsRequest,
  FetchPatientsSuccessPayload,
} from '../types/fetchAllPatientsTypes';
import {
  ForwardPatientFailure,
  ForwardPatientFailurePayload,
  ForwardPatientRequest,
  ForwardPatientSuccess,
  ForwardPatientSuccessPayload,
} from '../types/forwardPatientTypes';

export const fetchPatientsRequest = (): FetchPatientsRequest => ({
  type: patientsActionTypes.FETCH_PATIENTS_REQUEST,
});

export const fetchPatientsSuccess = (
  payload: FetchPatientsSuccessPayload,
): FetchPatientsSuccess => ({
  type: patientsActionTypes.FETCH_PATIENTS_SUCCESS,
  payload,
});

export const fetchPatientsFailure = (
  payload: FetchPatientsFailurePayload,
): FetchPatientsFailure => ({
  type: patientsActionTypes.FETCH_PATIENTS_FAILURE,
  payload,
});

/**
 * Forward patient types actions
 * @param payload
 */

export const forwardPatientRequest = (
  payload: IPatient,
): ForwardPatientRequest => ({
  type: singlePatientForwardTypes.FORWARD_PATIENT_REQUEST,
  payload,
});

export const forwardPatientSuccess = (
  payload: ForwardPatientSuccessPayload,
): ForwardPatientSuccess => ({
  type: singlePatientForwardTypes.FORWARD_PATIENT_REQUEST_SUCCESS,
  payload,
});

export const forwadPatientFailure = (
  payload: ForwardPatientFailurePayload,
): ForwardPatientFailure => ({
  type: singlePatientForwardTypes.FORWARD_PATIENT_REQUEST_FAIL,
  payload,
});

/***
 * Single Patient  actions
 */

import {
  FetchSinglePatientFailure,
  FetchSinglePatientSuccess,
  FetchSinglePatientFailurePayload,
  FetchSinglePatientRequest,
  FetchSinglePatientSuccessPayload,
} from '../types/singlePatientTypes';

export const fetchSinglePatientsRequest = (
  id: string,
): FetchSinglePatientRequest => ({
  type: singlePatientActionTypes.FETCH_SINGLE_PATIENT_REQUEST,
  id,
});

export const fetchSinglePatientsSuccess = (
  payload: FetchSinglePatientSuccessPayload,
): FetchSinglePatientSuccess => ({
  type: singlePatientActionTypes.FETCH_SINGLE_PATIENT_SUCCESS,
  payload,
});

export const fetchSinglePatientsFailure = (
  payload: FetchSinglePatientFailurePayload,
): FetchSinglePatientFailure => ({
  type: singlePatientActionTypes.FETCH_SINGLE_PATIENT_PATIENT_FAILURE,
  payload,
});
