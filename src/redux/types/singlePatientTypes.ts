import { IPatient } from '../../models/IPatient';
import { singlePatientActionTypes } from '../action types';

export interface SinglePatientState {
  pending: boolean;
  patient: IPatient | null;
  error: string | null;
}

export interface FetchSinglePatientSuccessPayload {
  patient: IPatient;
}

export interface FetchSinglePatientFailurePayload {
  error: string;
}

export interface FetchSinglePatientRequest {
  type: typeof singlePatientActionTypes.FETCH_SINGLE_PATIENT_REQUEST;
  id: string;
}

export type FetchSinglePatientSuccess = {
  type: typeof singlePatientActionTypes.FETCH_SINGLE_PATIENT_SUCCESS;
  payload: FetchSinglePatientSuccessPayload;
};

export type FetchSinglePatientFailure = {
  type: typeof singlePatientActionTypes.FETCH_SINGLE_PATIENT_PATIENT_FAILURE;
  payload: FetchSinglePatientFailurePayload;
};

export type SinglePatientActions =
  | FetchSinglePatientRequest
  | FetchSinglePatientSuccess
  | FetchSinglePatientFailure;
