import { IPatient } from '../../models/IPatient';
import { singlePatientForwardTypes } from '../action types';

export interface ForwardPatientState {
  isPatientForwardPending: boolean;
  isPatientForward: boolean;
  isPatientForwardError: string | null;
}

export interface ForwardPatientSuccessPayload {
  isPatientForward: boolean;
}
export interface ForwardPatientFailurePayload {
  error: string;
}

export interface ForwardPatientRequest {
  type: typeof singlePatientForwardTypes.FORWARD_PATIENT_REQUEST;
  payload: IPatient;
}

export type ForwardPatientSuccess = {
  type: typeof singlePatientForwardTypes.FORWARD_PATIENT_REQUEST_SUCCESS;
  payload: ForwardPatientSuccessPayload;
};

export type ForwardPatientFailure = {
  type: typeof singlePatientForwardTypes.FORWARD_PATIENT_REQUEST_FAIL;
  payload: ForwardPatientFailurePayload;
};

export type ForwardPatientActions =
  | ForwardPatientFailure
  | ForwardPatientSuccess
  | ForwardPatientRequest;
