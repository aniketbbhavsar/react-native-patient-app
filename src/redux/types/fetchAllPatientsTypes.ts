import { IPatients } from '../../models/IPatients';
import { patientsActionTypes } from '../action types';

export interface PatientsState {
  pending: boolean;
  patients: IPatients[];
  error: string | null;
}

export interface FetchPatientsSuccessPayload {
  patients: IPatients[];
}

export interface FetchPatientsFailurePayload {
  error: string;
}

export interface FetchPatientsRequest {
  type: typeof patientsActionTypes.FETCH_PATIENTS_REQUEST;
}

export type FetchPatientsSuccess = {
  type: typeof patientsActionTypes.FETCH_PATIENTS_SUCCESS;
  payload: FetchPatientsSuccessPayload;
};

export type FetchPatientsFailure = {
  type: typeof patientsActionTypes.FETCH_PATIENTS_FAILURE;
  payload: FetchPatientsFailurePayload;
};

export type PatientsActions =
  | FetchPatientsRequest
  | FetchPatientsSuccess
  | FetchPatientsFailure;
