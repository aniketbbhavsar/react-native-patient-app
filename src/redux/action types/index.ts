export enum patientsActionTypes {
  FETCH_PATIENTS_REQUEST = 'FETCH_PATIENTS_REQUEST',
  FETCH_PATIENTS_SUCCESS = 'FETCH_PATIENTS_SUCCESS',
  FETCH_PATIENTS_FAILURE = 'FETCH_PATIENTS_FAILURE',
}

export enum singlePatientActionTypes {
  FETCH_SINGLE_PATIENT_REQUEST = 'FETCH_SINGLE_PATIENT_REQUEST',
  FETCH_SINGLE_PATIENT_SUCCESS = ' FETCH_SINGLE_PATIENT_SUCCESS',
  FETCH_SINGLE_PATIENT_PATIENT_FAILURE = 'FETCH_SINGLE_PATIENT_PATIENT_FAILURE',
}

export enum singlePatientForwardTypes {
  FORWARD_PATIENT_REQUEST = ' FORWARD_PATIENT_REQUEST',
  FORWARD_PATIENT_REQUEST_SUCCESS = 'FETCH_PATIENTS_REQUEST_SUCCESS',
  FORWARD_PATIENT_REQUEST_FAIL = 'FETCH_PATIENTS_REQUEST_FAIL',
}
