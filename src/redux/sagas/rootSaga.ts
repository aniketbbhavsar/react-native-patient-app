import { all, fork, takeEvery } from 'redux-saga/effects';
import getlAllPatientsSaga from './GetAllPatients';
import getSinglePatientSaga from './GetSinglePatients';
import forwardPatientSaga from './ForwardPatient';

export function* rootSaga() {
  yield all([
    fork(getlAllPatientsSaga),
    fork(getSinglePatientSaga),
    fork(forwardPatientSaga),
  ]);
}
