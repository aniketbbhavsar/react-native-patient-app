import axios from 'axios';
import { all, call, put, take } from 'redux-saga/effects';
import { IPatient } from '../../models/IPatient';
import { forwadPatientFailure, forwardPatientSuccess } from '../actions';

import { singlePatientForwardTypes } from '../action types/';
import { GET_BY_ALL_PATIENTS } from '../../constants';

const forwardPatient = (payload: IPatient) => {
  const { id } = payload;
  return axios(`${GET_BY_ALL_PATIENTS}/${id}/forward`, {
    method: 'POST',
    headers: { 'Content-Type': 'application/json' },
  });
};

function* postNewPatientHandlerSaga(payload: IPatient): any {
  try {
    const response = yield call(forwardPatient, payload);

    if (response.status === 200) {
      yield put(
        forwardPatientSuccess({
          isPatientForward: true,
        }),
      );
    } else if (response.status === 500) {
      throw Error('Something Went Wrong' + response.status);
    }
  } catch (error) {
    console.log(error);
    yield put(
      forwadPatientFailure({
        error: (error as Error).message,
      }),
    );
  }
}

function* forwardPatientSaga() {
  const { payload } = yield take(
    singlePatientForwardTypes.FORWARD_PATIENT_REQUEST,
  );

  yield all([
    singlePatientForwardTypes.FORWARD_PATIENT_REQUEST,
    postNewPatientHandlerSaga(payload),
  ]);
}

export default forwardPatientSaga;
