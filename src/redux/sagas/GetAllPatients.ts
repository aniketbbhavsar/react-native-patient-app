import axios from 'axios';
import { all, call, put, takeLatest } from 'redux-saga/effects';
import { IPatients } from '../../models/IPatients';
import { fetchPatientsFailure, fetchPatientsSuccess } from '../actions';
import { patientsActionTypes } from '../action types';
import { GET_BY_ALL_PATIENTS } from '../../constants';

const getPatients = () => {
  return axios.get<IPatients[]>(GET_BY_ALL_PATIENTS);
};

function* fetchPatientsHandlerSaga(): any {
  try {
    const response = yield call(getPatients);
    yield put(
      fetchPatientsSuccess({
        patients: response.data,
      }),
    );
  } catch (error) {
    yield put(
      fetchPatientsFailure({
        error: (error as Error).message,
      }),
    );
  }
}

function* getlAllPatientsSaga() {
  yield all([
    takeLatest(
      patientsActionTypes.FETCH_PATIENTS_REQUEST,
      fetchPatientsHandlerSaga,
    ),
  ]);
}

export default getlAllPatientsSaga;
