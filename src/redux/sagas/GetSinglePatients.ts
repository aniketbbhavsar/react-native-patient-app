import axios from 'axios';
import { all, call, put, take, takeEvery } from 'redux-saga/effects';
import { IPatient } from '../../models/IPatient';
import {
  fetchSinglePatientsFailure,
  fetchSinglePatientsSuccess,
} from '../actions';
import { singlePatientActionTypes } from '../action types/';
import { GET_BY_ALL_PATIENTS } from '../../constants';

const getSinglePatient = (id: string) => {
  return axios.get<IPatient>(`${GET_BY_ALL_PATIENTS}/${id}`);
};

function* fetchSinglePatientsHandlerSaga(id: string): any {
  try {
    const response = yield call(getSinglePatient, id);
    yield put(
      fetchSinglePatientsSuccess({
        patient: response.data,
      }),
    );
  } catch (error) {
    yield put(
      fetchSinglePatientsFailure({
        error: (error as Error).message,
      }),
    );
  }
}

function* getSinglePatientSaga() {
  const { id } = yield take(
    singlePatientActionTypes.FETCH_SINGLE_PATIENT_REQUEST,
  );

  yield all([
    singlePatientActionTypes.FETCH_SINGLE_PATIENT_REQUEST,
    fetchSinglePatientsHandlerSaga(id),
  ]);
}

export default getSinglePatientSaga;
