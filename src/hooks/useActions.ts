import { useDispatch } from 'react-redux';
import { bindActionCreators } from 'redux';
import { actionCreators } from '../redux/store';

export const useAction = () => {
  const dispatch = useDispatch();

  return bindActionCreators(actionCreators, dispatch);
  // bindaction back obj all different action cretors all actions provide dispatch
};
