import { StyleSheet } from 'react-native';

export const globalStyles = StyleSheet.create({
  container: {
    flex: 1,
    textAlign: 'center',
    padding: 25,
  },
  titleText: {
    fontFamily: 'Cochin',
    fontSize: 20,
    color: '#333',
  },
  paragraph: {
    marginVertical: 8,
    lineHeight: 20,
  },
  successText: {
    textAlign: 'center',
    fontSize: 20,
    color: 'green',
  },
  FailText: {
    textAlign: 'center',
    fontSize: 20,
    color: 'red',
  },
  subTitile: {
    fontWeight: 'bold',
  },
});
