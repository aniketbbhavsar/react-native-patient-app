import React from 'react';
import { View, Text } from 'react-native';
import { globalStyles } from '../../styles/global';
import { AntDesign } from '@expo/vector-icons';

interface IMessage {
  message: string;
}

const ErrorComponent: React.FC<IMessage> = ({ message }) => {
  return (
    <View>
      <Text style={globalStyles.FailText}> {message}</Text>
      <Text style={globalStyles.FailText}> Please try again </Text>
      <Text style={globalStyles.FailText}>
        <AntDesign name="closecircle" size={24} color="#EC6A7C" />
      </Text>
    </View>
  );
};

export default ErrorComponent;
