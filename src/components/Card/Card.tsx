import React from 'react';
import { View } from 'react-native';
import cardStyle from './CardStyles';

interface CardContent {
  children?: React.ReactNode;
}

const Card: React.FC<CardContent> = (props: CardContent) => {
  return (
    <View style={cardStyle.card}>
      <View style={cardStyle.cardContent}>{props.children}</View>
    </View>
  );
};

export default Card;
