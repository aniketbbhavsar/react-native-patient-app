import React, { useEffect, useState } from 'react';
import { Text, FlatList, TouchableOpacity } from 'react-native';
import { StackNavigationProp } from '@react-navigation/stack';
import { useTypedSelector } from '../hooks/useTypedSelector';
import { useAction } from '../hooks/useActions';
import { useNavigation } from '@react-navigation/native';
import { globalStyles } from '../styles/global';
import Card from '../components/Card/Card';
import Loader from '../components/Loader/Loader';
import { AntDesign } from '@expo/vector-icons';
import ErrorComponent from '../components/Error/ErrorComponent';

export type RootStackParamList = {
  Details: { id: string };
};

const PatientsList: React.FC = () => {
  const { fetchPatientsRequest } = useAction();
  const navigation = useNavigation<StackNavigationProp<RootStackParamList>>();
  const allPatientsState = useTypedSelector((state) => state.patients);
  const { patients, pending, error } = allPatientsState;

  useEffect(() => {
    fetchPatientsRequest();
  }, []);

  return (
    <>
      {pending && <Loader />}
      {error && <ErrorComponent message={error} />}
      {patients && !!patients.length && (
        <FlatList
          data={patients}
          renderItem={({ item }) => (
            <TouchableOpacity
              onPress={() => navigation.navigate('Details', { id: item.id })}
            >
              <Card>
                <Text style={globalStyles.titleText}>
                  <Text style={globalStyles.subTitile}> Name: </Text>
                  {item.name}{' '}
                  {item.isForwarded ? (
                    <Text>
                      <AntDesign name="checkcircle" size={24} color="#53F2AF" />
                    </Text>
                  ) : (
                    <Text>
                      <AntDesign name="closecircle" size={24} color="#EC6A7C" />
                    </Text>
                  )}
                </Text>
                <Text style={globalStyles.titleText}>
                  <Text style={globalStyles.subTitile}> Gender: </Text>{' '}
                  {item.gender}
                </Text>
              </Card>
            </TouchableOpacity>
          )}
        />
      )}
    </>
  );
};

export default PatientsList;
