import React, { useEffect } from 'react';
import { Text, View, Button } from 'react-native';
import { globalStyles } from '../styles/global';
import { useTypedSelector } from '../hooks/useTypedSelector';
import { useRoute } from '@react-navigation/native';
import { useAction } from '../hooks/useActions';
import Loader from '../components/Loader/Loader';
import Card from '../components/Card/Card';
import { IPatient, Event } from '../models/IPatient';
import { SinglePatientState } from '../redux/types/singlePatientTypes';
import ErrorComponent from '../components/Error/ErrorComponent';
import { ForwardPatientState } from '../redux/types/forwardPatientTypes';
import { AntDesign } from '@expo/vector-icons';

interface Id {
  id: string;
}

const PatientDetails = () => {
  const route = useRoute();
  const { fetchSinglePatientsRequest, forwardPatientRequest } = useAction();
  const { id } = route.params as Id;
  const state: SinglePatientState = useTypedSelector(
    (state) => state.singlePatient,
  );
  const { patient, pending, error } = state;
  const isForwardState: ForwardPatientState = useTypedSelector(
    (state) => state.patientForward,
  );
  const {
    isPatientForward,
    isPatientForwardPending,
    isPatientForwardError,
  } = isForwardState;

  useEffect(() => {
    fetchSinglePatientsRequest(id);
  }, [id]);

  const handlePatientForward = (patient: IPatient | null): void => {
    if (patient !== null) {
      forwardPatientRequest(patient);
    }
  };

  return (
    <>
      {isPatientForwardPending && <Loader />}

      {!pending && isPatientForward && (
        <View>
          <Text style={globalStyles.successText}>
            {' '}
            Patient Forward Successfully{' '}
          </Text>
          <Text style={globalStyles.successText}>
            <AntDesign name="checkcircle" size={24} color="#53F2AF" />
          </Text>
        </View>
      )}

      {isPatientForwardError?.length && (
        <ErrorComponent message={isPatientForwardError} />
      )}

      {pending && isPatientForwardPending && (error?.length as number) > 0 ? (
        <Loader />
      ) : (
        !isPatientForwardError?.length &&
        !isPatientForward && (
          <View style={globalStyles.container}>
            <Card>
              <Text style={globalStyles.titleText}>
                {' '}
                <Text style={globalStyles.subTitile}>Name: </Text>{' '}
                {patient?.name}
              </Text>
              <Text style={globalStyles.titleText}>
                <Text style={globalStyles.subTitile}> Gender: </Text>{' '}
                {patient?.gender}
              </Text>

              {patient?.events.map((patientEvent: Event, index: number) => (
                <View key={index}>
                  <Text style={globalStyles.titleText}>
                    {' '}
                    <Text style={globalStyles.subTitile}>Event: </Text>
                    {patientEvent.text}
                  </Text>
                  <Text style={globalStyles.titleText}>
                    <Text style={globalStyles.subTitile}> Date: </Text>
                    {
                      new Date(patientEvent.createdAt)
                        .toISOString()
                        .split('T')[0]
                    }
                  </Text>
                </View>
              ))}
              {!patient?.isForwarded ? (
                <Button
                  title="Forward Patient"
                  onPress={() => handlePatientForward(patient)}
                />
              ) : null}
            </Card>
          </View>
        )
      )}
    </>
  );
};

export default PatientDetails;
