import React from 'react';
import store from './src/redux/store/store';
import 'react-native-gesture-handler';
import PatientsList from './src/screens/PatientsList';
import PatientDetails from './src/screens/PatientDetails';
import { Provider } from 'react-redux';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';

const Stack = createNativeStackNavigator();

export default function App() {
  return (
    <Provider store={store}>
      <NavigationContainer>
        <Stack.Navigator>
          <Stack.Screen
            name="Patients List"
            component={PatientsList}
            options={{
              headerTitle: 'Patient List',
              headerStyle: {
                backgroundColor: '#eee',
              },
              headerTintColor: '#444',
            }}
          />
          <Stack.Screen
            name="Details"
            component={PatientDetails}
            options={{
              headerTitle: 'Details',
              headerStyle: {
                backgroundColor: '#eee',
              },
              headerTintColor: '#444',
            }}
          />
        </Stack.Navigator>
      </NavigationContainer>
    </Provider>
  );
}
